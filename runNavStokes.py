import sys
import os
from dolfin import *

if len(sys.argv) < 5:
    fileOUTc = "results_concentration/myMeshXMLSym_c.pvd"
else:
    fileOUTc = sys.argv[4]

if len(sys.argv) < 4:
    fileOUTp = "results_pressure/myMeshXMLSym_p.pvd"
else:
    fileOUTp = sys.argv[3]

if len(sys.argv) < 3:
    fileOUTu = "results_velocity/myMeshXMLSym_u.pvd"
else:
    fileOUTu = sys.argv[2]

if len(sys.argv) < 2:
    fileIN = "myMeshXML.xml"
else:
    fileIN = sys.argv[1]

print "Reading info file"

fileINFO = fileIN[:-3]+"info"

position = []
length =[]

with open(fileINFO, 'r') as readFile:
    for i, line in enumerate(readFile):
        if i == 0:
            line = line.strip()
            columns = line.split()
            numParticles = int(columns[3])
        elif i >= 3 and i <= 5:
            line = line.strip()
            columns = line.split()
            position.append(float(columns[1]))
        elif i >= 7 and i <= 9:
            line = line.strip()
            columns = line.split()
            length.append(float(columns[1]))

print "Reading info file ... done"

print "Checking if paths available"

dir_p = os.path.dirname(fileOUTp)
if not os.path.exists(dir_p):
    os.makedirs(dir_p)

dir_u = os.path.dirname(fileOUTu)
if not os.path.exists(dir_u):
    os.makedirs(dir_u)
    
dir_c = os.path.dirname(fileOUTc)
if not os.path.exists(dir_c):
    os.makedirs(dir_c)

print "Checking if paths available ... done"

print "Beginning operations for simulation"

# choose file with mesh to solve NS
mesh = Mesh(fileIN)

print "Mesh file reading ... done"

# pressure is driving force for flow
Pin = Constant(0.0007)
Pout= Constant(0.0)

print "Setting pressures ... done"

# define noslip condition
noslip = Constant((0.0, 0.0, 0.0))
ny = Constant(0.01)

V = VectorFunctionSpace(mesh, "Lagrange", 2)
Q = FunctionSpace(mesh, "Lagrange", 1)
MF= Q*V
info(MF)

dmf = TrialFunction(MF)
mf = Function(MF)
test = TestFunction(MF)
q,v = split(test)
p,u = split(mf)
dp,du = split(dmf)

#plot(mesh)
# interactive()

set_log_level(1)

eps = 1e-6
class Dokola(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

class Vtok(SubDomain):
    def inside(self, x, on_boundary):
        return x[2] < (position[2] + eps) and on_boundary

class Vytok(SubDomain):
    def inside(self, x, on_boundary):
        return x[2] > (position[2] + length[2]-eps) and on_boundary

steny = Dokola()
vtok = Vtok()
vytok = Vytok()

boundaries = FacetFunction("size_t", mesh)
boundaries.set_all(0)

steny.mark(boundaries, 3)
vtok.mark(boundaries, 1)
vytok.mark(boundaries, 2)

bc1 = DirichletBC(MF.sub(0), Pin, boundaries, 1)
bc2 = DirichletBC(MF.sub(0), Pout, boundaries, 2)
bc3 = DirichletBC(MF.sub(1), noslip, boundaries, 3)
bcs = [bc1, bc2, bc3]

#plot(boundaries)
#interactive()
ds = Measure('ds', domain=mesh, subdomain_data=boundaries)

# sys.exit(0)

ufile = File(fileOUTu)
pfile = File(fileOUTp)

# Define variational form
Fp = inner(div(u),q)*dx
Fv = (inner(2*ny*grad(u),grad(v)) + inner(grad(u)*u,v) + inner(
    grad(p),v))*dx # + inner(u.dx(2),v)*ds(1) - inner(
    # u.dx(2),v)*ds(2)

F = Fp+Fv

J = derivative(F, mf)#, dmf)

problem = NonlinearVariationalProblem(F, mf, bcs, J)
solver = NonlinearVariationalSolver(problem)

prm = solver.parameters
prm['newton_solver']['linear_solver'] = 'gmres'
prm['newton_solver']['preconditioner'] = 'petsc_amg'
prm['newton_solver']['absolute_tolerance'] = 1E-10
prm['newton_solver']['relative_tolerance'] = 1E-7
prm['newton_solver']['maximum_iterations'] = 10
prm['newton_solver']["error_on_nonconvergence"] = True
#prm['newton_solver']['absolute_tolerance'] = 1E-8
#prm['newton_solver']['relative_tolerance'] = 1E-7
#prm['newton_solver']['maximum_iterations'] = 10
#prm['newton_solver']['linear_solver'] = 'gmres'
# prm['newton_solver']['linear_solver'] = 'gmres'
#parameters["form_compiler"]["representation"] = "quadrature"
#parameters["form_compiler"]["quadrature_degree"] = 5
#prm["relative_tolerance"] = 1e-6

solver.solve()
u = mf.split()[1]
p = mf.split()[0]

ufile << u

File('saved_u_slowdens.xml') << mf
#u_sav = u.vector().array().reshape(len(u.vector()),1)
#savemat('saved_u', { 'u_sav': u_sav }, oned_as='column')

pfile << p

#plot(mf.split()[0])
#plot(mf.split()[1])


#mesh2 = Mesh(fileIN[:-4]+"_dense.xml")
#plot(mesh2)

mesh2 = Mesh(fileIN[:-4]+"_filled.xml")
V_ext = VectorFunctionSpace(mesh2, "Lagrange", 2)
C = FunctionSpace(mesh2, "Lagrange", 1)
info(C)

class InitialCondition(Expression):
    def __init__(self):
        self.a = 4.0
    def eval(self,values,x):
        if ( between(x[2], (position[2],position[2]+0.5)) ):
            values[0] = self.a
        else:
            values[0] = 0.
    def value_shape(self):
        return()

class InflowCondition(Expression):
    def __init__(self,t):
        self.t = t
    def eval(self,values,x):
        if ( t<30*dt ):
            values[0] = 4.
        else:
            values[0] = 0.
    def value_shape(self):
        return()

c  = Function(C)
c_ = TestFunction(C)

# c_init = Constant(0.0)
c_init = InitialCondition()
c0 = project(c_init,C)

D0 = Constant(1e-7)
D1 = Constant(1e-12)
dt = 500.
cdt = Constant(dt)
idt = Constant(1/dt)
t_end = 1500*dt+0.1*dt

class ExtrapolateVelocity(Expression):
    def __init__(self,u):
        self.u = u
    def eval(self, values, x):
        try:
            u(x)
        except:
            values[0] = 0.0
            values[1] = 0.0
            values[2] = 0.0
        else:
            values[0] = u(x)[0]
            values[1] = u(x)[1]
            values[2] = u(x)[2]
    def value_shape(self):
        return (3,)

aux = ExtrapolateVelocity(u)
u_ext = project(aux,V_ext)
File('u_ext_slowdens.xml') << u_ext

class Solid(SubDomain):
    def inside(self, x, on_boundary):
        try:
            u(x)
        except:
            return True
        else:
            return False
            
solid = Solid()

# Initialize mesh function for interior domains
domains = CellFunction("size_t",mesh2)
domains.set_all(0)
solid.mark(domains,1)

# Define new measures associated with the interior domains and
# exterior boundaries
dx = Measure('dx', domain=mesh2, subdomain_data=domains)

boundaries2 = FacetFunction("size_t", mesh2)
boundaries2.set_all(0)
steny.mark(boundaries2, 3)
vtok.mark(boundaries2, 1)
vytok.mark(boundaries2, 2)
ds = Measure('ds', domain=mesh2, subdomain_data=boundaries2)

cbc = []#[DirichletBC(C, 0.0, boundaries2,1),DirichletBC(C, 0.0, boundaries2,3)]

c_in = Expression('0.0')
#c_in = InflowCondition(0.0)

# variational forms
L =  idt*inner(c-c0,c_)*dx - inner(-D1*grad(c)+u_ext*c,grad(c_))*dx(1) \
      - inner(-D0*grad(c)+u_ext*c,grad(c_))*dx(0) \
      + inner(-D1*c.dx(0) + u_ext[2]*c,c_)*ds(2) + inner(-u_ext[2]*c_in,c_)*ds(1)


J2 = derivative(L, c)#, dmf)

problem2 = NonlinearVariationalProblem(L, c, cbc, J2)
solver2  = NonlinearVariationalSolver(problem2)

prm = solver2.parameters
prm['newton_solver']['absolute_tolerance'] = 1E-12
prm['newton_solver']['relative_tolerance'] = 1E-17
prm['newton_solver']['maximum_iterations'] = 10
prm['newton_solver']['linear_solver'] = 'gmres'
prm['newton_solver']['preconditioner'] = 'petsc_amg'
prm['newton_solver']["error_on_nonconvergence"] = True

cfile = File(fileOUTc)
# cOutFile = File('ObtokTelesa1/cOUT/cOut.txt')
# Time-stepping
t = dt

while t < t_end:
    print 'time = ', t/60, 'min'
    # c_in = InflowCondition(t)

    solve(L == 0, c)
    
    #plot(c)
    cfile<<c0
    c0.assign(c)
    out_conc = assemble(c*ds(2))
    print 'Outflow concentration = ',out_conc

    t += dt


cfile<<c0

# Hold plots
#interactive()