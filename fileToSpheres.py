########################################################################################################################
#   Author:         Vaclav Pelc                                                                                        #
#   Name:           fileToSpeheres.py                                                                                  #
#   Last edited:    2016-04-26                                                                                         #
#   Info:           The algorithm takes two input arguments - file in and file out. If they are not defined, sets      #
#                   default values. Then finds position and radius of particles from file in and writes geometry (.geo)#
#                   file out in Gmsh format. After that, Gmsh is called to create mesh file from geometry file. Info   #
#                   file is created to provide information about particles and system geometry in short.               #
########################################################################################################################


# import onelab as ol
import sys
from subprocess import call
# from dolfin_utils import meshconvert

if len(sys.argv) < 3:
    fileOUT = "myFileOut.geo"
else:
    fileOUT = sys.argv[2]

if len(sys.argv) < 2:
    fileIN = "myFileIn.out"
else:
    fileIN = sys.argv[1]

# print sys.argv[1]
# print type(sys.argv[1])

print "Input file set to "+fileIN
print "Geometry output file set to "+fileOUT

xs = []
ys = []
zs = []
rs = []

# fileIN = "output.out"
print "Reading file "+fileIN

with open(fileIN,'r') as readFile:
    for line in readFile:
        line = line.strip()
        columns = line.split()
        xs.append(float(columns[0]))
        ys.append(float(columns[1]))
        zs.append(float(columns[2]))
        rs.append(float(columns[3]))

print "Number of particles is "+str(len(xs))

# rs = [x*1.05 for x in rs]



ls = 0.1
ls2 = 0.1
addSpace = 0.000002
addLength = 1

print "Ls is set to "+str(ls)
print "Writing "+fileOUT

lenxs = len(xs)

def writeGeoFile(fileOUT, xs,ys, zs, rs, ls, ls2, addSpace, addLength, lenxs, filled):
    right = max([x + r for x, r in zip(xs, rs)])
    left = min([x - r for x, r in zip(xs, rs)])
    up = max([y + r for y, r in zip(ys, rs)])
    down = min([y - r for y, r in zip(ys, rs)])
    front = max([z + r for z, r in zip(zs, rs)])
    back = min([z - r for z, r in zip(zs, rs)])
    with open(fileOUT,'w') as writeFile:
        writeFile.write("// gmsh file\n\n")
        writeFile.write("ls = "+str(ls)+";\n")
        writeFile.write("ls2 = " + str(ls2) + ";\n\n")
        for i in range(lenxs):
            # Write points
            writeFile.write("Point("+str(i*7 + 1)+") = {"+str(xs[i])+", "+str(ys[i])+", "+str(zs[i])+", ls};\n")
            writeFile.write("Point(" + str(i*7 + 2) + ") = {" + str(xs[i]+rs[i]) + ", " + str(ys[i]) + ", " + str(zs[i])\
                            + ", ls};\n")
            writeFile.write("Point(" + str(i*7 + 3) + ") = {" + str(xs[i]) + ", " + str(ys[i]+rs[i]) + ", " + str(zs[i])\
                            + ", ls};\n")
            writeFile.write("Point(" + str(i*7 + 4) + ") = {" + str(xs[i]-rs[i]) + ", " + str(ys[i]) + ", " + str(zs[i])\
                            + ", ls};\n")
            writeFile.write("Point(" + str(i*7 + 5) + ") = {" + str(xs[i]) + ", " + str(ys[i]-rs[i]) + ", " + str(zs[i])\
                            + ", ls};\n")
            writeFile.write("Point(" + str(i*7 + 6) + ") = {" + str(xs[i]) + ", " + str(ys[i]) + ", " + str(zs[i]+rs[i])\
                            + ", ls};\n")
            writeFile.write("Point(" + str(i*7 + 7) + ") = {" + str(xs[i]) + ", " + str(ys[i]) + ", " + str(zs[i]-rs[i])\
                            + ", ls};\n\n")

            # Write circles
            writeFile.write("Circle("+str(i*18+1)+") = {"+str(i*7+3)+", "+str(i*7+1)+", "+str(i*7+2)+"};\n")
            writeFile.write("Circle(" + str(i * 18 + 2) + ") = {" + str(i * 7 + 2) + ", " + str(i * 7 + 1) + ", " + str(
                i * 7 + 5) + "};\n")
            writeFile.write("Circle(" + str(i * 18 + 3) + ") = {" + str(i * 7 + 3) + ", " + str(i * 7 + 1) + ", " + str(
                i * 7 + 4) + "};\n")
            writeFile.write("Circle(" + str(i * 18 + 4) + ") = {" + str(i * 7 + 4) + ", " + str(i * 7 + 1) + ", " + str(
                i * 7 + 5) + "};\n")
            writeFile.write("Circle(" + str(i * 18 + 5) + ") = {" + str(i * 7 + 5) + ", " + str(i * 7 + 1) + ", " + str(
                i * 7 + 6) + "};\n")
            writeFile.write("Circle(" + str(i * 18 + 6) + ") = {" + str(i * 7 + 6) + ", " + str(i * 7 + 1) + ", " + str(
                i * 7 + 3) + "};\n")
            writeFile.write("Circle(" + str(i * 18 + 7) + ") = {" + str(i * 7 + 5) + ", " + str(i * 7 + 1) + ", " + str(
                i * 7 + 7) + "};\n")
            writeFile.write("Circle(" + str(i * 18 + 8) + ") = {" + str(i * 7 + 7) + ", " + str(i * 7 + 1) + ", " + str(
                i * 7 + 3) + "};\n\n")

            # Write line loops
            writeFile.write("Line Loop("+str(i*18+9)+") = {"+str(i*18+1)+", "+str(i*18+2)+", "+str(i*18+5)+", "+str(
                i*18+6)+"};\n")
            writeFile.write("Line Loop("+str(i*18+10)+") = {"+str(i*18+1)+", "+str(i*18+2)+", "+str(i*18+7)+", "+str(
                i*18+8)+"};\n")
            writeFile.write("Line Loop("+str(i*18+11)+") = {"+str(i*18+3)+", "+str(i*18+4)+", "+str(i*18+7)+", "+str(
                i*18+8)+"};\n")
            writeFile.write("Line Loop("+str(i*18+12)+") = {"+str(i*18+3)+", "+str(i*18+4)+", "+str(i*18+5)+", "+str(
                i*18+6)+"};\n\n")

            # Write ruled surfaces
            writeFile.write("Ruled Surface("+str(i*18+13)+") = {"+str(i*18+9)+"};\n")
            writeFile.write("Ruled Surface("+str(i*18+14)+") = {"+str(i*18+10)+"};\n")
            writeFile.write("Ruled Surface("+str(i*18+15)+") = {"+str(i*18+11)+"};\n")
            writeFile.write("Ruled Surface("+str(i*18+16)+") = {"+str(i*18+12)+"};\n\n")

            # Write surface loop
            writeFile.write("Surface Loop("+str(i*18+17)+") = {"+str(i*18+13)+", "+str(i*18+14)+", "+str(i*18+15)+", "+str(
                i*18+16)+"};\n\n")

            # Write volume
            if (filled):
                writeFile.write("Volume("+str(i*18+18)+") = {"+str(i*18+17)+"};\n\n")

        # Write cuboid
        writeFile.write("Point("+str(lenxs*7+1)+") = {"+str(left-addSpace)+", "+str(down-addSpace)+", "+str(
            back-addLength)+", ls2};\n")
        writeFile.write("Point(" + str(lenxs*7 + 2) + ") = {" + str(right+addSpace) + ", " + str(
            down-addSpace) + ", " + str(back-addLength) + ", ls2};\n")
        writeFile.write("Point(" + str(lenxs*7 + 3) + ") = {" + str(right+addSpace) + ", " + str(
            up+addSpace) + ", " + str(back-addLength) + ", ls2};\n")
        writeFile.write("Point(" + str(lenxs*7 + 4) + ") = {" + str(left-addSpace) + ", " + str(
            up+addSpace) + ", " + str(back-addLength) + ", ls2};\n")
        
        writeFile.write("Point("+str(lenxs*7+5)+") = {"+str(left-addSpace)+", "+str(down-addSpace)+", "+str(
            front+addLength)+", ls2};\n")
        writeFile.write("Point(" + str(lenxs*7 + 6) + ") = {" + str(right+addSpace) + ", " + str(
            down-addSpace) + ", " + str(front+addLength) + ", ls2};\n")
        writeFile.write("Point(" + str(lenxs*7 + 7) + ") = {" + str(right+addSpace) + ", " + str(
            up+addSpace) + ", " + str(front+addLength) + ", ls2};\n")
        writeFile.write("Point(" + str(lenxs*7 + 8) + ") = {" + str(left-addSpace) + ", " + str(
            up+addSpace) + ", " + str(front+addLength) + ", ls2};\n\n")
        
        writeFile.write("Line(" + str(lenxs * 18 + 1) + ") = {" + str(lenxs*7+1) + ", " + str(lenxs*7+2) + "};\n")
        writeFile.write("Line(" + str(lenxs * 18 + 2) + ") = {" + str(lenxs * 7 + 2) + ", " + str(lenxs * 7 + 3) + "};\n")
        writeFile.write("Line(" + str(lenxs * 18 + 3) + ") = {" + str(lenxs * 7 + 3) + ", " + str(lenxs * 7 + 4) + "};\n")
        writeFile.write("Line(" + str(lenxs * 18 + 4) + ") = {" + str(lenxs * 7 + 4) + ", " + str(lenxs * 7 + 1) + "};\n\n")

        writeFile.write("Line Loop("+str(lenxs*18 + 5)+") = {"+ str(lenxs * 18 + 1) +", "+ str(lenxs * 18 + 2) +", "+ str(
            lenxs * 18 + 3) +", "+ str(lenxs * 18 + 4) +"};\n\n")

        writeFile.write("Plane Surface("+str(lenxs*18+5)+") = {"+str(lenxs*18+5)+"};\n\n")
        
        writeFile.write("Line(" + str(lenxs * 18 + 6) + ") = {" + str(lenxs*7+6) + ", " + str(lenxs*7+5) + "};\n")
        writeFile.write("Line(" + str(lenxs * 18 + 7) + ") = {" + str(lenxs * 7 + 7) + ", " + str(lenxs * 7 + 6) + "};\n")
        writeFile.write("Line(" + str(lenxs * 18 + 8) + ") = {" + str(lenxs * 7 + 8) + ", " + str(lenxs * 7 + 7) + "};\n")
        writeFile.write("Line(" + str(lenxs * 18 + 9) + ") = {" + str(lenxs * 7 + 5) + ", " + str(lenxs * 7 + 8) + "};\n\n")
        
        writeFile.write("Line Loop("+str(lenxs*18 + 10)+") = {"+ str(lenxs * 18 + 6) +", "+ str(lenxs * 18 + 9) +", "+ str(
            lenxs * 18 + 8) +", "+ str(lenxs * 18 + 7) +"};\n\n")

        writeFile.write("Plane Surface("+str(lenxs*18+10)+") = {"+str(lenxs*18+10)+"};\n\n")
        
        writeFile.write("Line(" + str(lenxs * 18 + 11) + ") = {" + str(lenxs * 7 + 2) + ", " + str(lenxs * 7 + 6) + "};\n")
        writeFile.write("Line(" + str(lenxs * 18 + 12) + ") = {" + str(lenxs * 7 + 5) + ", " + str(lenxs * 7 + 1) + "};\n\n")
        
        writeFile.write("Line Loop("+str(lenxs*18 + 13)+") = {"+ str(lenxs * 18 + 1) +", "+ str(lenxs * 18 + 11) +", "+ str(
            lenxs * 18 + 6) +", "+ str(lenxs * 18 + 12) +"};\n\n")
        
        writeFile.write("Plane Surface("+str(lenxs*18+13)+") = {"+str(lenxs*18+13)+"};\n\n")
        
        writeFile.write("Line(" + str(lenxs * 18 + 14) + ") = {" + str(lenxs * 7 + 3) + ", " + str(lenxs * 7 + 7) + "};\n")
        writeFile.write("Line(" + str(lenxs * 18 + 15) + ") = {" + str(lenxs * 7 + 6) + ", " + str(lenxs * 7 + 2) + "};\n\n")
        
        writeFile.write("Line Loop("+str(lenxs*18 + 16)+") = {"+ str(lenxs * 18 + 2) +", "+ str(lenxs * 18 + 14) +", "+ str(
            lenxs * 18 + 7) +", "+ str(lenxs * 18 + 15) +"};\n\n")
        
        writeFile.write("Plane Surface("+str(lenxs*18+16)+") = {"+str(lenxs*18+16)+"};\n\n")
        
        writeFile.write("Line(" + str(lenxs * 18 + 17) + ") = {" + str(lenxs * 7 + 4) + ", " + str(lenxs * 7 + 8) + "};\n")
        writeFile.write("Line(" + str(lenxs * 18 + 18) + ") = {" + str(lenxs * 7 + 7) + ", " + str(lenxs * 7 + 3) + "};\n\n")
        
        writeFile.write("Line Loop("+str(lenxs*18 + 19)+") = {"+ str(lenxs * 18 + 3) +", "+ str(lenxs * 18 + 17) +", "+ str(
            lenxs * 18 + 8) +", "+ str(lenxs * 18 + 18) +"};\n\n")
        
        writeFile.write("Plane Surface("+str(lenxs*18+19)+") = {"+str(lenxs*18+19)+"};\n\n")
        
        writeFile.write("Line(" + str(lenxs * 18 + 20) + ") = {" + str(lenxs * 7 + 1) + ", " + str(lenxs * 7 + 5) + "};\n")
        writeFile.write("Line(" + str(lenxs * 18 + 21) + ") = {" + str(lenxs * 7 + 8) + ", " + str(lenxs * 7 + 4) + "};\n\n")
        
        writeFile.write("Line Loop("+str(lenxs*18 + 22)+") = {"+ str(lenxs * 18 + 4) +", "+ str(lenxs * 18 + 20) +", "+ str(
            lenxs * 18 + 9) +", "+ str(lenxs * 18 + 21) +"};\n\n")
        
        writeFile.write("Plane Surface("+str(lenxs*18+22)+") = {"+str(lenxs*18+22)+"};\n\n")
        
        writeFile.write("Surface Loop("+str(lenxs*18+23)+") = {"+str(lenxs*18+5)+", "+str(lenxs*18+13)+", "+str(
            lenxs*18+16)+", "+str(lenxs*18+19)+", "+str(lenxs*18+22)+", "+str(lenxs*18+10)+"};\n\n")
        
        writeFile.write("Coherence;\n\n")
        
        # Volume and compound volume
        writeFile.write("Volume("+str(lenxs*18+24)+") = {"+str(lenxs*18+23))
        for i in range(lenxs):
            writeFile.write(", "+str(i*18+17))
        writeFile.write("};\n\n")

writeGeoFile(fileOUT, xs, ys, zs, rs, ls, ls2, addSpace, addLength, lenxs, False)

fileOUTFilled = fileOUT[:-4]+"_filled.geo"
writeGeoFile(fileOUTFilled, xs, ys, zs, rs, ls, ls2, addSpace, addLength, lenxs, True) 
# fileOUTDense = fileOUT[:-4]+"_dense.geo"
# ls = 0.05
# ls2 = 0.1
# writeGeoFile(fileOUTDense, xs, ys, zs, rs, ls, ls2, addSpace, addLength, lenxs, False)    
# print type(fileOUT[:-3]+"msh")

print "Writing "+fileOUT+" file done"

fileMSH = fileOUT[:-3]+"msh"
fileMSHFilled = fileOUT[:-4]+"_filled.msh"
# fileMSHDense = fileOUT[:-4]+"_dense.msh"
print "Beginning writing "+fileMSH

call(["gmsh", "-3", fileOUT, "-o", fileMSH])
call(["gmsh", "-3", fileOUTFilled, "-o", fileMSHFilled])
# call(["gmsh", "-3", fileOUTDense, "-o", fileMSHDense])

fileINFO = fileOUT[:-3]+"info"

print "Beginning writing info file"

with open(fileINFO,'w') as writeFile:
    right = max([x + r for x, r in zip(xs, rs)])
    left = min([x - r for x, r in zip(xs, rs)])
    up = max([y + r for y, r in zip(ys, rs)])
    down = min([y - r for y, r in zip(ys, rs)])
    front = max([z + r for z, r in zip(zs, rs)])
    back = min([z - r for z, r in zip(zs, rs)])
    
    writeFile.write("Number of particles: "+str(lenxs)+"\n\n")

    writeFile.write("Cuboid shape info:\n")
    writeFile.write("xstart:    " + str(left - addSpace) + "\n")
    writeFile.write("ystart:    " + str(down - addSpace) + "\n")
    writeFile.write("zstart:    " + str(back - addLength) + "\n\n")

    writeFile.write("xlength:   "+str(right+addSpace-(left-addSpace))+"\n")
    writeFile.write("ylength:   "+str(up+addSpace-(down-addSpace))+"\n")
    writeFile.write("zlength:   "+str(front+addLength-(back-addLength))+"\n")

print "Writing info file done"

